// Constructor de las bicicletas
var Bicicleta = function(id, color, modelo, ubicacion){
  this.id = id;
  this.color = color;
  this.modelo = modelo;
  this.ubicacion = ubicacion;
}

// Funcion que devuelve la bicicleta
Bicicleta.prototype.toString = function (){
  return 'id: ' + this.id + "| color: " + this.color;
}

// Arreglo de bicicletas /simula un BD
Bicicleta.allBicis = [];
Bicicleta.add = function(aBici){
  Bicicleta.allBicis.push(aBici);
}

// Eliminar
Bicicleta.findById = function(aBiciId){
  var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
  if (aBici)
    return aBici;
  else
    throw new Error(`No existe una bicicleta con el id ${aBiciId}`);
}

Bicicleta.removeById = function(aBiciId){
  for (var index = 0; index < Bicicleta.allBicis.length; index++) {
    if (Bicicleta.allBicis[index].id == aBiciId) {
      Bicicleta.allBicis.splice(index, 1);
      break;
    }
  }
}

// var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6012424, -58.3861497]);
// var b = new Bicicleta(2, 'verde', 'montaña', [-34.596932, -58.3808287]);

// Bicicleta.add(a);
// Bicicleta.add(b);

// Exporta la funcion para que otras clase puedan usarlo
module. exports = Bicicleta;