var  Bicicleta = require('../../models/bicicleta');

// Resetea el arreglo antes de cada test
beforeEach(() => {
  Bicicleta.allBicis = [];
});

// ANCHOR ---------- TEST_1
describe('Bicicleta.allBicis', () => {
  it('comienza vacia', () => {
    expect(Bicicleta.allBicis.length).toBe(0);
  });
});

// ANCHOR ---------- TEST_2
describe('Bicicleta.add', () => {
  it('agregamos una', () => {
    expect(Bicicleta.allBicis.length).toBe(0);

    var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6012424, -58.3861497]);
    Bicicleta.add(a);

    expect(Bicicleta.allBicis.length).toBe(1);
    expect(Bicicleta.allBicis[0]).toBe(a);
  });
});

// ANCHOR ---------- TEST_3
describe('Bicicleta find id', () => {
  it('Debe devolver la bici con id 1', ()=>{
    expect(Bicicleta.allBicis.length).toBe(0);
    var aBici  = new Bicicleta(1, 'rojo', 'urbana', [4.7127434,-74.1161973]);
    var aBici2 = new Bicicleta(2, 'verde', 'montaña', [4.7127434,-74.1161973]);
    Bicicleta.add(aBici);
    Bicicleta.add(aBici2);

    var targetBici = Bicicleta.findById(1);
    expect(targetBici.id).toBe(1);
    expect(targetBici.color).toBe(aBici.color);
    expect(targetBici.modelo).toBe(aBici.modelo);
  });
});

// ANCHOR ---------- TEST_4
describe('Bicicleta remove id', () => {
  it('Debe remover la bici con id 1', ()=>{
    expect(Bicicleta.allBicis.length).toBe(0);
    var aBici  = new Bicicleta(1, 'rojo', 'urbana', [4.7127434,-74.1161973]);
    var aBici2 = new Bicicleta(2, 'verde', 'montaña', [4.7127434,-74.1161973]);
    Bicicleta.add(aBici);
    Bicicleta.add(aBici2);

    var targetBici = Bicicleta.findById(1);

    Bicicleta.removeById(1);

    expect(Bicicleta.allBicis.length).toBe(1);
    expect(targetBici.id).toBe(1);
    expect(targetBici.color).toBe(aBici.color);
    expect(targetBici.modelo).toBe(aBici.modelo);
  });
});

